
const validateName = (data) => {
    const regexForName = /^[a-zA-Z ]{2,30}$/
    if (regexForName.test(data)) {
        return true
    } else {
        return false
    }
}
const validateEmail = (email) => {
    const regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/
    return regex.test(email)
}
const validateStreet = (street) => {
    if (street.length >= 10 && street.length <= 50) {
        return true;
    } else {
        return false;
    }

}
const validateCVC = (cvc) =>{
    const regex = /^[0-9]{3}$/
    return regex.test(cvc)
}
const validateZip = (zip) =>{
    const regex = /^[0-9]{6}$/
    return regex.test(zip)
}
const validateCardNumber = (cardNumber) =>{
    const regex  = /^(?:(4[0-9]{12}(?:[0-9]{3})?)|(5[1-5][0-9]{14})|(6(?:011|5[0-9]{2})[0-9]{12})|(3[47][0-9]{13})|(3(?:0[0-5]|[68][0-9])[0-9]{11})|((?:2131|1800|35[0-9]{3})[0-9]{11}))$/
    if(regex.test(cardNumber))
    {
        return true
    }else{
        return false
    }
}
const validateMonth = (month) =>{
    
    const regex = /^[0][1-9]|[1][0-2]$/
    return regex.test(month)
}
const validateYear = (year) =>{
    const regex = /^[0-9]{1}$/
    return regex.test(year)
}

const validateForm = (event) => {
    const firstName = document.getElementById("firstName")
    const lastName = document.getElementById("lastName")
    const email = document.getElementById("email")
    const street = document.getElementById("street")
    const city = document.getElementById("city")
    const state = document.getElementById("state")
    const zip = document.getElementById("zip")
    const cardNumber = document.getElementById("cardNumber")
    const month = document.getElementById("month")
    const year = document.getElementById("year")

    // console.log(firstName.value +" "+ lastName.value+ " "+ email.value+ " " +street.value + " " +city.value+" " +state.value
    //  +" " +zip.value+" " +cardNumber.value+ " " + month.value+ " " +year.value);
    if (firstName.value == "") {
        alert("Please enter your first name ")
        firstName.focus();
        return false;
    } else if (!(firstName.value != "" && validateName(firstName.value))) {
        alert("Plaese enter valid first name ")
        firstName.focus()
        firstName.value = ""
        return false;
    } else if (lastName.value == "") {
        alert("Please enter your last name")
        lastName.focus()
        return false;
    } else if (!(lastName.value != "" && validateName(lastName.value))) {
        alert("Please enter valid last name")
        lastName.focus()
        lastName.value = ""
        return false;
    } else if (street.value == "") {
        alert("Please enter your street information")
        street.focus()
        return false;
    } else if (!(street.value != "" && validateStreet(street.value))) {
        alert("Street minimum lenght 10 and max lenght 50")
        street.focus();
        return false;
    }else if(city.value == "")
    {
        alert("Please enter you city name")
        city.focus()
        return false
    }else if ( !(city.value != "" && validateName(city.value))){
        alert("Please enter valid city name")
        city.focus()
        return false;
    }else if(zip.value == "")
    {
        alert("Please enter your zip")
        zip.focus()
        return false
    }else if(!(zip.value != ""  && validateZip(zip.value))){
        alert("Please enter valid zip")
        zip.focus()
        return false;

    }else if (email.value == "") {
        alert("Please enter your email")
        email.focus();
        return false;
    } else if (!(email.value != "" && validateEmail(email.value))) {
        alert("Please enter valid email address")
        email.focus()
        return false;
    }else if(cardNumber.value  ==  "")
    {
        alert("Please enter your card number")
        cardNumber.focus()
        return false;
    }else if(!(cardNumber.value != "" && validateCardNumber(cardNumber.value))){
        alert("Please enter valid card number")
        cardNumber.focus()
        return false;
    }else if(month.value == ""){
        alert("Please Enter expiry month")
        month.focus()
        return false;
    }else if(! (month.value != ""  && validateMonth(month.value)))
    {
        alert("Please enter valid expiry month")
        month.focus()
        month.value = ""
        return false
    }else if(year.value == "")
    {
        alert("Please enter expiry year")
        year.focus()
        return false
    }else if(year.value != "" && validateYear(year.value)){
        alert("Please enter valid expiry year")
        year.focus()
        year.value = ""
        return false
    }else if(cvc.value == "")
    {
        alert("Please enter your cvc number")
        cvc.focus()
        return false
    }else if(!(cvc.value != "" && validateCVC(cvc.value)))
    {
        alert("please enter valid cvc number")
        cvc.focus()
        cvc.value = ""
        return false
    }
    else {
        alert("Every thing is perfec")
        return false;
    }


}